from titleInfo import extract_title_from_box,extract_title_no_box
from StudentInfo import extract_studentName, extract_programName
from Committee import extract_chair, extract_committee

if __name__=="__main__":

        path = r'.\Prescheduling\Brynn.xml'
        
        with open(path) as f:
                file = f.read()
                
        title = extract_title_from_box(file)
        if title == '':
                title = extract_title_no_box(file)
        print(title)
        f = open(r'./Template/Template.xml')
        #,errors='ignore'       ,encoding="utf-8"
        pp = f.read()
        f.close()
        print('DissertationTitle' in pp)
        pp = pp.replace('DissertationTitle',title)
        
        studentName = extract_studentName(file)
        pp = pp.replace('StudentName',studentName)
        programName = extract_programName(file)
        pp = pp.replace('Department',programName)
        chairName = extract_chair(file)
        chairName += ' (Committee Chair)'

        pp = pp.replace('committeechair',chairName)
        committee = extract_committee(file)
        for i in range(len(committee)):
                pp = pp.replace('Member{}'.format(i+1),committee[i])
                
        with open('DissertationVideo.xml','w') as f:
                f.write(pp)
        
        
        
        

