# The following code reads the XML file of the prescheduling form and extract the title.
# The word document has to be converted to XML 2003.
#Some of the form have the title outside of the title box, so the following code needs to be altered.
from titleInfo import extract
import re

def extract_wt_tag(x):

    pattern = re.compile('<w:t>')
    starts = pattern.finditer(x)
    pattern = re.compile('</w:t>')
    ends = pattern.finditer(x)
    starts = [i.span()[0] for i in starts]
    ends = [i.span()[0] for i in ends]
    
    return (starts,ends)

path = r'.\Prescheduling\Raquib.xml'

with open(path) as f:
        file = f.read()
def extract_studentName(file):
    
    i=file.find('>Name:<')
    j=file[i:].find('</w:p')
    studentName = file[i:i+j]    
    starts,ends = extract_wt_tag(studentName)
    studentName = studentName[starts[-1]+5:ends[-1]]
    return studentName


def extract_programName(file):
    i = file.find('Program Name')
    j = file[i:].find('Academic')
    if j == -1:
        j = file[i:].find('College')
    
    programName = file[i+15:i+j-15] #added 15 to both ends to remove extra tags that do not have pairs in the other list.

    starts,ends = extract_wt_tag(programName)
    program = []

    for i in range(len(starts)):
        text = programName[starts[i]+5:ends[i]].strip()
        if '<' in text or text=='':
            continue
    
        else:
            program.append(text)
    return "".join(program)


        
    
