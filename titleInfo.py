# The following code reads the XML file of the prescheduling form and extract the title.
# The word document has to be converted to XML 2003.
#Some of the form have the title outside of the title box, so the following code needs to be altered.

import re
import xml.etree.ElementTree as ET


def extract_sentence(x):
        
        start = x.find('<w:t>')
        end = x.find('</w:t>')
        # only the closing tag '<a:i/>' is used in word XML for italic
        # but the xml in powerpoint for italic is different. the following takes care of it
        if '<w:i/>' in x:
                return '<a:r><a:rPr lang="en-US" i="1"  dirty="0" err="1"><a:solidFill><a:srgbClr val="FFC50B"/></a:solidFill></a:rPr><a:t>'+x[start+5:end]+'</a:t></a:r>'
        elif '</w:pict>' in x or '<w:br/>' in x or 'tab/' in x:
                return ''
        else:
                return '<a:r><a:rPr dirty="0" err="1"><a:solidFill><a:srgbClr val="FFC50B"/></a:solidFill></a:rPr><a:t>'+x[start+5:end]+'</a:t></a:r>'

def extract(text):

        pattern = re.compile('</w:r>')
        matches = pattern.finditer(text)
        ends = [0]
        for match in matches:
                ends.append(match.span()[1])
        final_title = []
        for i in range(len(ends)-1):
                t = text[ends[i]:ends[i+1]]
                final_title.append(extract_sentence(t))
                #print(extract_sentence(t),'\n\n')
                    
        return("".join(final_title))

        
def extract_title_from_box(file):

        supervisor = file.find('Supervisory')
        file = file[:supervisor]
        
        start = file.find('<w:txbxContent>')
        end = file.find('</w:txbxContent>')
        if start == -1:
                return ""
        title = file[start+15:end]
        
        return(extract(title))
        
def extract_title_no_box(file):

        supervisor = file.find('lternative')
        if supervisor == -1:
                supervisor = file.find('upervisory')
        file = file[:supervisor+30]
               
        pattern = re.compile('itle:')
        matches = pattern.finditer(file)
        for match in matches:
                dissertation = match.span()[0]
                
        end_dissertation = file.find('upervisory')
        
        file = file[dissertation+5:end_dissertation]
        
        return extract(file)


if __name__=="__main__":

        path = r'.\Prescheduling\Raquib.xml'
        title = extract_title_from_box(path)
        if title == '':
                title = extract_title_no_box(path)
        print(title)
        with open('Presentation2_worked.xml') as f:
                pp = f.read()
        pp = pp.replace('TitleHere',title)

        with open('DissertationVideo.xml','w') as f:
                f.write(pp)
                
        
        
        

