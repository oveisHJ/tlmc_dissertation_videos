# The following code reads the XML file of the prescheduling form and extract the title.
# The word document has to be converted to XML 2003.
#Some of the form have the title outside of the title box, so the following code needs to be altered.
from titleInfo import extract
from StudentInfo import extract_wt_tag
import re


def extract_chair(file):
    
    i = file.find('Chair')
    j = file[i:].find('</w:p')
    chairName = file[i:i+j]
    starts,ends = extract_wt_tag(chairName)
    chairName = chairName[starts[-1]+5:ends[-1]]
    return chairName.strip()


def extract_committee(file):
    i = file.find('Chair')
    j = file[i:].find('</w:txbxContent>')
    committee = file[i:i+j]
    starts,ends = extract_wt_tag(committee)
    committee = committee[starts[-1]+5:ends[-1]]
    committee = committee.strip().split(',')
    committee = [i.strip() for i in committee]
    
    if 'and' in committee[-1]:
        committee[-1] = committee[-1].replace('and','') 
        committee[-1] = committee[-1].strip()
        
    return committee


if __name__=='__main__':

    path = r'.\Prescheduling\Brian.xml'

    with open(path) as f:
        file = f.read()
    chairName = extract_chair(file)
    print(chairName)
    committee = extract_committee(file)
    print(committee)
